# BackEnd Starter Kit

BackEnd Starter Kit should be used for any generic project. Once empty repo is created for new project, we should connect it with BackEnd Starter remote and decide how to merge it to master branch. Just after remote is added and merging are done, you are ready to start codding and deploying to QA/UAT environment, no extra steps from developers should be taken.

## Getting Started


Decide what kind of backend service you need. You have 2 available options as of today: Midtier and Microservice. Also WIP on Microclient.

* Midtier: Express nodejs application to work with FrontEnd and Microservice. 
```
It should deal with SAML passport strategy. 
We should supply roles/permissions from Midtier to FrontEnd (TBD).
Also Midtier should double check roles/permission as well (TBD).
Midtier can have its own database, in non microservice architecture.
Midtier rule of thumb is to call Microservice for data.
```
* Microservice: Express nodejs application to work with Database.
```
It should deal with additional security via TBD technology.
Microservice should not deal with SAML.
Microservice is a datasource for Midtier.
Midtier can't be datasource for Microservice.
One Microservice can be datasource for another Microservice.
```
* Microclient (TODO): Non Express nodejs application to work with Microservice.
```
Midtier app1 and Microservice app2 can both contact Microservice app3 via request utility.
If new version of app3 is released, then app1 and app2 should be notified, it is a challenge.
Microclient will be released together with Microservice to notify app1 and app2 for any updates of app3.
Microclient deals with request utility to work with Microservice.
Microclient provides high level API to app1 and app2, so that app1 and app2 should not deal with request utility anymore.
```

### Prerequisites

Any developer should have access to the following repositories.

* git@bitbucket.org:oliverwymantechssg/ngpd-merceros-starterkit-be.git
* git@bitbucket.org:oliverwymantechssg/ngpd-merceros-common-be.git


### Installing

* Install node, npm.
* Install git.
* Install Postman or curl.
* Install mongodb.
* Install Robomongo.
* Create repository for your project.

### Midtier bootstrapping
If you decided to create midtier type, please follow steps bellow. In case you want Microservice, please skip to its section.

* Create repository and clone it.
```
> git clone git@bitbucket.org:oliverwymantechssg/project_name_mt.git
> cd project_name_mt
```

* Add starter remote and fetch it.
```
> git remote add starter git@bitbucket.org:oliverwymantechssg/ngpd-merceros-starterkit-be.git
> git fetch starter
```
* Make sure you still have empty project_name_mt folder.
* Now we are ready to borrow code from starter kit, so let's use merge
```
> git merge starter/midtier
```
* Make sure you have project structure in project_name_mt folder.
* Open your IDE and search for app-name-change-me. Replace each app-name-change-me with project_name_mt (TODO, we will use yeoman generator). Note: keep app-name-change-me-test as project_name_mt-test, app-name-change-me.sid as project_name_mt.sid.
* Run npm install.
```
> npm install
```
* Start mongodb (if you haven't done it) and your backend application
```
> sudo mongod
> npm run start:dev
```
#### Midtier example routes.
Open Postman or use curl. Please walk through examples bellow. Feel free to check results in Robomongo.

* Example of healthcheck.
```
> curl -XGET http://localhost:3201/healthcheck -i

> Response:
HTTP/1.1 200 OK
X-Powered-By: Express
Access-Control-Allow-Origin: http://localhost:3000
Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE
Access-Control-Allow-Credentials: true
Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Expires
Surrogate-Control: no-store
Cache-Control: no-store, no-cache, must-revalidate, proxy-revalidate
Pragma: no-cache
Expires: 0
Content-Type: text/html; charset=utf-8
Content-Length: 2
ETag: W/"2-4KoCHiHd29bYzs7HHpz1ZA"
Date: Thu, 25 May 2017 10:35:27 GMT
Connection: keep-alive
```

* Example of usercheck route. This is session management check.
```
> curl -XGET http://localhost:3201/usercheck

> Response Body: {"data":{"authenticated":false,"authorized":false}}
```

* Example of Car create API. Please remove this API from your project, this is just an example.
```
> curl -H "Content-Type: application/json" -XPOST -d '{"brand":"audi","model":"A5"}' http://localhost:3201/api/car

> Response Body: {"data":{"__v":0,"brand":"audi","model":"A5","_id":"5926b2253022d45789d7c1c2"}}
```

* Example of Car fetch API. Please remove this API from your project, this is just an example. Please specify car id from previous example.
```
> curl -XGET http://localhost:3201/api/car/5926b2253022d45789d7c1c2

> Response Body: {"data":{"__v":0,"brand":"audi","model":"A5","_id":"5926b2253022d45789d7c1c2"}}
```

* Example of Car update API. Please remove this API from your project, this is just an example. Please specify car _id from previous example.
```
> curl -H "Content-Type: application/json" -XPUT -d '{"_id":"5926b2253022d45789d7c1c2", "brand":"audi","model":"A7"}' http://localhost:3201/api/car

> Response Body: {"data":{"n":1,"nModified":1,"ok":1}}
```

* Example of Car delete API. Please remove this API from your project, this is just an example. Please specify car id from previous example.
```
> curl -XDELETE http://localhost:3201/api/car/5926b2253022d45789d7c1c2

> Response Body: {"data":{"n":1,"ok":1}}
```

### Microservice bootstrapping
If you decided to create microservice type, please follow steps.

* Create repository and clone it.
```
> git clone git@bitbucket.org:oliverwymantechssg/project_name_ms.git
> cd project_name_ms
```

* Add starter remote and fetch it.
```
> git remote add starter git@bitbucket.org:oliverwymantechssg/ngpd-merceros-starterkit-be.git
> git fetch starter
```
* Make sure you still have empty project_name_ms folder.
* Now we are ready to borrow code from starter kit, so let's use merge
```
> git merge starter/microservice
```
* Make sure you have project structure in project_name_ms folder.
* Open your IDE and search for app-name-change-me-ms. Replace each app-name-change-me-ms with project_name_ms (TODO, we will use yeoman generator). Note: keep app-name-change-me-ms-test as project_name_ms-test.
* Run npm install.
```
> npm install
```
* Start mongodb (if you haven't done it) and your backend application.
```
> sudo mongod
> npm run start:dev
```

#### Microservice example routes.
* Open Postman or use curl. Please walk through examples bellow. Feel free to check results in Robomongo. Note: we should not have 'usercheck' route in microservice. TODO: we should git rid of it.

* Example of healthcheck.

```
> curl -XGET http://localhost:3204/healthcheck -i

> Response:
HTTP/1.1 200 OK
X-Powered-By: Express
Surrogate-Control: no-store
Cache-Control: no-store, no-cache, must-revalidate, proxy-revalidate
Pragma: no-cache
Expires: 0
Content-Type: text/html; charset=utf-8
Content-Length: 2
ETag: W/"2-4KoCHiHd29bYzs7HHpz1ZA"
Date: Thu, 25 May 2017 12:18:36 GMT
Connection: keep-alive
```

* Example of Car create API. Please remove this API from your project, this is just an example.
```
> curl -H "Content-Type: application/json" -XPOST -d '{"brand":"audi","model":"A5"}' http://localhost:3204/api/car

> Response Body: {"data":{"__v":0,"brand":"audi","model":"A5","_id":"5926cc0bf2986d6159210eef"}}
```

* Example of Car fetch API. Please remove this API from your project, this is just an example. Please specify car id from previous example.
```
> curl -XGET http://localhost:3204/api/car/5926cc0bf2986d6159210eef

> Response Body: {"data":{"_id":"5926cc0bf2986d6159210eef","brand":"audi","model":"A5","__v":0}}
```

* Example of Car update API. Please remove this API from your project, this is just an example. Please specify car _id from previous example.
```
> curl -H "Content-Type: application/json" -XPUT -d '{"_id":"5926cc0bf2986d6159210eef", "brand":"audi","model":"A7"}' http://localhost:3204/api/car

> Response Body: {"data":{"n":1,"nModified":1,"ok":1}}
```

* Example of Car delete API. Please remove this API from your project, this is just an example. Please specify car id from previous example.
```
> curl -XDELETE http://localhost:3204/api/car/5926cc0bf2986d6159210eef

> Response Body: {"data":{"n":1,"ok":1}}
```

### Midtier to Microservice and Microservice to Microservice.
In this part we explain how midtier should talk to microservice. Note: microservice talk to microservice in the same manner, so for brevity we skip ms-to-ms example.

Before we show some examples, please keep the following terminology.

* MT is Midtier.
* MS is Microservice.
* Pass Through API. MT/MS calls another MS using Pass Through API, so that MT/MS can't modify responce from called MS. We use this approach when there is no sensetive data to pass upstairs, we pass data as it was returned by source MS.
* Proxy API. MT/MS calls another MS using Proxy API, then MT/MS aggregate responce, remove sensetive data, add more data and pass responce upstairs.

#### Prerequisites for MT/MS to MS examples.

* In case if you went through steps to create midtier app and microservice app, we are good to reuse them now. Please go to Romobongo and remove project_name_mt and project_name_ms databases.
* please kill your ms and mt express applications and start them again, this will recreate databases. Please see below.
```
> cd project_name_mt
> npm run start:dev
> cd ..
> cd project_name_ms
> npm run start:dev
```

#### Midtier calls Microservice through Pass Through API example.
* We have midtier on http://localhost:3201
* We have microservice on http://localhost:3204
* Lets create a Car on microservice side.
```
> curl -H "Content-Type: application/json" -XPOST -d '{"brand":"audi","model":"A5"}' http://localhost:3204/api/car

> Response Body: {"data":{"__v":0,"brand":"audi","model":"A5","_id":"5926e03f611ccf688bfa4573"}}
```
* Lets fetch this Car using midtier pass through API. Please supply id from the previous call.
```
> curl -XGET http://localhost:3201/api/example/car/5926e03f611ccf688bfa4573

> Response Body: {"data":{"_id":"5926e03f611ccf688bfa4573","brand":"audi","model":"A5","__v":0}}
```
* Lets fetch the same Car using microservice direct call, just for the demo. Please supply id from the previous call. Please see identical responces.
```
> curl -XGET http://localhost:3204/api/car/5926e03f611ccf688bfa4573

> Response Body: {"data":{"_id":"5926e03f611ccf688bfa4573","brand":"audi","model":"A5","__v":0}}
```
* Please go to 'src/router.js' to see how pass through is configurated, below example. Everything route to /example is going to microservice in pass through fastion.
```
const passThroughRoute = require('./pass-through');
const allRoutes = require('./controller');

module.exports = (rootRouter) => {
  // EXAMPLE OF PASS THROUGH
  rootRouter.use('/api/example', passThroughRoute);

  // EXAMPLE OF OWN CONTROLLER FROM MID TIER
  rootRouter.use('/api', allRoutes);
};
```
* Configuration which microservice should be called on /example route you may find in 'src/config.js', feel free to config as many as you want.
```
  ...
  // EXAMPLES
  // Pass Through Microservices
  api_url_microservice_pass_through: () => envs('API_MICROSERVICE_PASS_THROUGH', 'http://localhost:3204'),
  ...
```
* You are encouraged to change /example route to some better naming, this is just an example.


#### Midtier calls Microservice as a Proxy API example.
* We have midtier on http://localhost:3201
* We have microservice on http://localhost:3204
* Lets create a Car on microservice side.
```
> curl -H "Content-Type: application/json" -XPOST -d '{"brand":"audi","model":"A5"}' http://localhost:3204/api/car

> Response Body: {"data":{"__v":0,"brand":"audi","model":"A5","_id":"5926e454611ccf688bfa4574"}}
```
* Lets fetch this Car using midtier proxy API. Please supply id from the previous call.
```
> curl -XGET http://localhost:3201/api/example2/car/5926e454611ccf688bfa4574

> Response Body: {"data":{"exampleData":{"data":{"_id":"5926e454611ccf688bfa4574","brand":"audi","model":"A5","__v":0}},"aggregation":"aggregation"}}
```
* Take a look at responce received. We have some editing, "aggregation": "aggregation". Below code snippet from midtier, which adds this data.
```
getExampleData(req, res, next) {
  const msSlag = req.params[0];
  this.proxyExampleService
    .getExampleData(msSlag)
    .then((exampleData) => {
      const agg = {
        exampleData,
        aggregation: 'aggregation'
      };
      return agg;
    })
    .then(this.responseWithResult(res))
    .catch(next);
}
```
* Configuration which microservice should be called on /example2 route you may find in 'src/config.js', feel free to config as many as you want.
```
  ...
  // Microservices to aggregate
  api_url_microservice_to_aggregate: () => envs('API_MICROSERVICE_TO_AGGREGATE', 'http://localhost:3204'),
  ...
```
### Microclient bootstrapping TODO


## Running the tests for each example TODO
* Please run the following command to run tests on local machine
```
> npm run test:local
```
* After successful run folder 'coverage' is created.