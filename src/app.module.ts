import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from './config/config.module';
import { StatusModule } from './status/status.module';
import { DevModule } from './dev/dev.module';
import { AuthModule } from './auth/auth.module'
  ;
@Module({
  imports: [ConfigModule, StatusModule, DevModule, AuthModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
}
