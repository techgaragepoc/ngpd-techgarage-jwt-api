export interface GenerateTokenRequest {
  username: string
  subject: string,
  payload: any
}