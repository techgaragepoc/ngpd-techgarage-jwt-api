import * as fs from 'fs';
import { ConfigService } from './config.service';
import * as Joi from 'joi';

describe('ConfigService', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });
  xdescribe('constructor()', () => {
    it('should read .env file if it exists', () => {
      const appName = Math.random().toString();

      const existsSync = jest.spyOn(fs, 'existsSync');
      const readFileSync = jest.spyOn(fs, 'readFileSync');

      existsSync.mockImplementation(jest.fn().mockReturnValue(true));
      readFileSync.mockImplementation(jest.fn().mockReturnValue(`APP_NAME=${appName}`));

      let configService;

      expect(() => {
        configService = new ConfigService();
      }).not.toThrow();

      expect(configService.get('APP_NAME')).toBe(appName);
      expect(existsSync).toHaveBeenCalledWith('.env');
      expect(readFileSync).toHaveBeenCalledWith('.env');
    });

    it('should not read .env file if it does not exist', () => {
      const appName = Math.random().toString();

      const existsSync = jest.spyOn(fs, 'existsSync');
      const readFileSync = jest.spyOn(fs, 'readFileSync');

      existsSync.mockImplementation(jest.fn().mockReturnValue(false));
      readFileSync.mockImplementation(jest.fn().mockReturnValue(`APP_NAME=${appName}`));

      let configService;

      expect(() => {
        configService = new ConfigService();
      }).not.toThrow();

      expect(configService.get('APP_NAME')).toBe('wealth-columbus-benefitcenter-main-be');
      expect(existsSync).toHaveBeenCalledWith('.env');
      expect(readFileSync).not.toHaveBeenCalled();
    });

    it('should throw Error if env file contains incorrect values', () => {
      const existsSync = jest.spyOn(fs, 'existsSync');
      const readFileSync = jest.spyOn(fs, 'readFileSync');

      existsSync.mockImplementation(jest.fn().mockReturnValue(true));
      readFileSync.mockImplementation(jest.fn().mockReturnValue(`NODE_ENV=${Math.random().toString()}`));

      let configService;

      expect(() => {
        configService = new ConfigService();
      }).toThrow();
    });
  });

  describe('get', () => {
    it('should return value from process.env', () => {
      const appName = Math.random().toString();
      process.env.APP_NAME = appName;

      const existsSync = jest.spyOn(fs, 'existsSync');
      const readFileSync = jest.spyOn(fs, 'readFileSync');
      existsSync.mockImplementation(jest.fn().mockReturnValue(true));
      readFileSync.mockImplementation(jest.fn().mockReturnValue(true));

      const configService = new ConfigService();

      const key = 'setUpEnvConfig';
      ConfigService[key]();

      const result = configService.get('APP_NAME');
      expect(result).toBe(appName);

      delete process.env.APP_NAME;
    });

    it('should throw if validation failed', () => {
      const validate = jest.spyOn(Joi, 'validate');
      validate.mockImplementation(jest.fn().mockReturnValue({error: {message: 'testError'}}));

      const key = 'validateInput';

      expect( () => {
        ConfigService[key]({});
      }).toThrow();
    });

    it('should call env.config', () => {
      const key = 'getEnvConfig';
      expect(ConfigService[key]()).not.toBe(null);
    });

    it('should return value from default config', () => {
      const existsSync = jest.spyOn(fs, 'existsSync');
      existsSync.mockImplementation(jest.fn().mockReturnValue(false));

      const configService = new ConfigService();

      expect(configService.get('APP_NAME')).not.toBe(null);
    });

    it('should throw if env key does not exist', () => {
      const existsSync = jest.spyOn(fs, 'existsSync');
      existsSync.mockImplementation(jest.fn().mockReturnValue(false));

      const configService = new ConfigService();

      expect(() => {
        configService.get(Math.random().toString());
      }).toThrow();
    });
  });
});
