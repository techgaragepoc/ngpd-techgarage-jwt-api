import * as dotenv from 'dotenv';
import * as Joi from 'joi';
import * as fs from 'fs';
import { Injectable } from '@nestjs/common';

export interface EnvConfig {
  [key: string]: string;
}

@Injectable()
export class ConfigService {
  private static readonly envConfig: EnvConfig = ConfigService.setUpEnvConfig();

  private static setUpEnvConfig() {
    let config = {};

    const fileCode = (process.env['NODE_ENV'] === 'prod') ? 'prod' : (process.env['NODE_ENV'] === 'qa') ? 'qa' : 'dev';
    const envFilePath = __dirname + `/../../env/.env.${fileCode}.properties`;

    // console.log('envFilePath', envFilePath, fs.existsSync(envFilePath), process.env['env'], process.env);

    if (fs.existsSync(envFilePath)) {
      config = dotenv.parse(fs.readFileSync(envFilePath));
      // console.log('envFilePath', config);
    }
    return ConfigService.validateInput(config);
  }

  static getEnvConfig() {
    return this.envConfig;
  }

  private static validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = ConfigService.getObjectSchema();

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  private static getObjectSchema(): Joi.ObjectSchema {
    return Joi.object({
      NODE_ENV: Joi.string()
        .valid(['dev', 'test', 'qa', 'uat', 'stg', 'prod'])
        .default('dev'),
      APP_PORT: Joi.number().default(3000),
      APP_NAME: Joi.string().default('ngpd-techgarage-jwt-api'),
      APP_VERSION_STRING: Joi.string().default('not specified'),
      APP_SESSION_SECRET: Joi.string().default('session-secret'),
      APP_COOKIE_SECURE: Joi.boolean().default(true),
      APP_CORS: Joi.array().default([
        'http://localhost:3000',
        'http://localhost:4200',
      ]),
      LOGGER_LEVEL: Joi.string().default('debug'),
      LOGGER_HANDLE_EXCEPTIONS: Joi.boolean().default(true),
      LOGGER_SERVICE: Joi.string().default('starter-main-be'),
      LOGGER_PRIVACY_LOG_REQUEST: Joi.boolean().default(false),
      ERROR_LOG_BROWSER_DETAILED: Joi.boolean().default(false),
      DEV_GET_CONFIG_ENABLED: Joi.boolean().default(true),
      DEV_GET_ENV_ENABLED: Joi.boolean().default(true),
      MONGO_URI: Joi.string().default('mongodb://localhost:27017/ngpd-techgarage-jwt-api'),
      JWT_API_KEY: Joi.string().default('2f7nGT30QZOO1FDCcpsU1F2PsZkisM5P'),
      JWT_GENERATE_HS256_ENDPOINT: Joi.string().default('http://pankajsabharwal-eval-test.apigee.net/login'),
      JWT_GENERATE_RS256_ENDPOINT: Joi.string().default('http://pankajsabharwal-eval-prod.apigee.net/jwt-rs256'),
      JWT_VERIFY_HS256_ENDPOINT: Joi.string().default('http://pankajsabharwal-eval-test.apigee.net/jwt-verify-hs256'),
      JWT_VERIFY_RS256_ENDPOINT: Joi.string().default('http://pankajsabharwal-eval-prod.apigee.net/jwt-verify-rs256'),
    });
  }

  get(key: string): any {
    if (process.env.hasOwnProperty(key)) {
      return process.env[key];
    } else if (typeof ConfigService.envConfig[key] !== 'undefined') {
      return ConfigService.envConfig[key];
    }
    throw new Error(`Missing config variable: ${key}`);
  }
}
