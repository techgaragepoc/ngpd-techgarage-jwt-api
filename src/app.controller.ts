import { Controller, Get } from '@nestjs/common';
import { ConfigService } from './config/config.service';
import { ApiProduces } from '@nestjs/swagger';

@Controller()
export class AppController {
  constructor(private readonly configService: ConfigService) {
  }

  @Get()
  @ApiProduces('text/plain')
  getInfo(): string {
    return `${this.configService.get('APP_NAME')}: ${this.configService.get('APP_VERSION_STRING')}`;
  }
}
