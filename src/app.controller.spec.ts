import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { ConfigService } from './config/config.service';
import { ConfigModule } from './config/config.module';

describe('AppController', () => {
  const configService = { get: (key) => undefined };
  let appController: AppController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      controllers: [AppController],
    })
      .overrideProvider(ConfigService)
      .useValue(configService)
      .compile();

    appController = module.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "app name and version string"', () => {
      const appName = Math.random().toString();
      const versionString = Math.random().toString();
      jest.spyOn(configService, 'get').mockImplementation((key) => {
        if (key === 'APP_VERSION_STRING') {
          return versionString;
        } else if (key === 'APP_NAME') {
          return appName;
        }
        return undefined;
      });
      expect(appController.getInfo()).toBe(`${appName}: ${versionString}`);
    });
  });
});
