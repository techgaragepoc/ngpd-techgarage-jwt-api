import { Injectable, HttpService, Header } from '@nestjs/common';
import { AxiosResponse, AxiosRequestConfig } from 'axios';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { TokenRequest } from './dto/token.request'
import { ConfigService } from '../config/config.service';

@Injectable()
export class AuthService {
  constructor(private readonly httpService: HttpService, private readonly configService: ConfigService) { }

  getHS256Token<T>(body: T, headers?: any) {
    return this.postServiceCall<T>(
      this.configService.get('JWT_GENERATE_HS256_ENDPOINT'),
      body,
      // { apiKey: this.configService.get('JWT_API_KEY') }
      [{ apikey: headers.apikey }, { 'Content-Type': 'application/x-www-form-urlencoded' }]
      // [{ apikey: headers.apikey }]
    );
  }

  getRS256Token<T>(body: T) {
    return this.postServiceCall<T>(this.configService.get('JWT_GENERATE_RS256_ENDPOINT'), body);
  }

  verifyHS256Token<T>(body: T, headers?: any) {
    // console.log('received headers', headers);

    return this.getServiceCall<T>(this.configService.get('JWT_VERIFY_HS256_ENDPOINT'),
      [{ apikey: headers.apikey }, { authorization: headers.authorization }]
    );
  }

  verifyRS256Token<T>(body: T, headers?: any) {
    return this.postServiceCall<T>(this.configService.get('JWT_VERIFY_RS256_ENDPOINT'), body, [{ jwt: headers.jwt }]);
  }

  /*
  postServiceCall<T>(endpoint: string, body: T, headers?: any) {
    // console.log('headers', headers);
    // console.log(endpoint);

    if (headers == null) {
      //   headers = new Headers();
      console.log('headers null', headers);
      headers = {};
    }

    const apiKey = headers.apikey;

    if (apiKey === null || apiKey === undefined) {
      // console.log(this.configService.get('JWT_API_KEY'), endpoint);
      // headers.append("apikey", "2f7nGT30QZOO1FDCcpsU1F2PsZkisM5P");
      // console.log('headers', headers);
      // headers = { ...headers, apikey: this.configService.get('JWT_API_KEY') };
      // console.log('body', body, 'headers', headers);      
      // .push({ "apikey": this.configService.get('JWT_API_KEY') });
    }

    // console.log('headers', headers);

    // headers.append("Content-Type", "application/x-www-form-urlencoded");

    // [
    //   ,
    //   {
    //     "key": "apikey",
    //     "value": "2f7nGT30QZOO1FDCcpsU1F2PsZkisM5P",
    //     "type": "text"
    //   }
    // ];

    return this.httpService.post<T>(endpoint, body, { headers: headers })
      .pipe(map(x => x.data as T))
      .toPromise();
  }*/

  postServiceCall<T>(endpoint: string, body: T, headers?: any[]) {
    return this.httpService.post<T>(endpoint, body, { headers: this.transformHeaders(headers) })
      .pipe(map(x => { return x.data as T; }))
      .toPromise();
  }

  getServiceCall<T>(endpoint: string, headers?: any[]) {
    // console.log('allHeaders', allHeaders);
    return this.httpService.get<T>(endpoint, { headers: this.transformHeaders(headers) })
      .pipe(map(x => { return x.data as T; }))
      .toPromise();
  }

  transformHeaders(headers?: any[]) {
    // console.log('post', headers.map(x => x));
    let allHeaders = {};

    // Make all header items inline
    if (headers) {
      headers.forEach(x => { allHeaders = Object.assign(allHeaders, x); })
    }

    return allHeaders;
  }
}
