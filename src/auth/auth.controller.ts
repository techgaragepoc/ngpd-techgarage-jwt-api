import { Body, Controller, Get, Post, Req, Res, HttpException, HttpStatus, Headers, Header } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { AuthService } from './auth.service';
import { ApiUseTags, ApiProduces } from '@nestjs/swagger';
import { GenerateTokenRequest, VerifyTokenRequest } from '../dto';

@Controller('api/auth')
// @ApiUseTags('auth')
export class AuthController {
  constructor(private readonly config: ConfigService, private readonly authService: AuthService) {
  }

  @Post('generate/hs256')
  // @Header('Content-Type', 'application/x-www-form-urlencoded')
  getHS256Token( @Body() body: GenerateTokenRequest, @Req() request, @Res() response, ) {
    this.authService.getHS256Token<GenerateTokenRequest>(body, request.headers)
      .then(result => {
        // console.log('response', result);
        response.json(result);
      })
      .catch(error => {
        response.status(500).send(error);
        // throw error;
      });
  }

  @Post('generate/rs256')
  getRS256Token( @Body() body: GenerateTokenRequest, @Res() response) {
    this.authService.getRS256Token<GenerateTokenRequest>(body)
      .then(result => {
        // console.log('response', result);
        response.json(result);
      })
      .catch(error => {
        // console.log('error', error);
        // response.error(error);
        response.status(500).send(error);
      });
  }

  @Get('verify/hs256')
  // @Post('verify/hs256')
  // @Header('Content-Type', 'application/x-www-form-urlencoded')
  verifyHS256Token( @Body() body: VerifyTokenRequest, @Req() request, @Res() response) {
    // console.log('got headers', request.headers);

    this.authService.verifyHS256Token<VerifyTokenRequest>(body, request.headers)
      .then(result => {
        // console.log('verufy reault', result);
        response.json(result);
      })
      .catch(error => {
        response.status(401).send({ error: 'Not a valid token' });
        // throw error;
      });
  }

  @Post('verify/rs256')
  verifyRS256Token( @Body() body: VerifyTokenRequest, @Req() request, @Res() response) {
    // console.log('called verifyRS256Token', request.headers);
    this.authService.verifyRS256Token<VerifyTokenRequest>(body, request.headers)
      .then(result => {
        // console.log('verufy reault', result);
        response.json(result);
      })
      .catch(error => {
        // console.log('error', error);
        response.status(401).send({ error: 'Not a valid token' });
        // throw error;
      });
  }
}
