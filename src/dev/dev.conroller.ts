import { Body, Controller, Get, Post, Req, Res, HttpException, HttpStatus, Headers } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { DevService } from './dev.service';
import { ApiUseTags, ApiProduces } from '@nestjs/swagger';
// import { Response } from 'express';
// import { TokenRequest } from '../dto/token.request'

@Controller('api/dev')
@ApiUseTags('dev')
export class DevController {
  constructor(private readonly config: ConfigService, private readonly devService: DevService) {
  }

  @Get('config')
  @ApiProduces('text/plain')
  getConfig( @Res() response) {
    if (this.config.get('DEV_GET_CONFIG_ENABLED')) {
      return response.json(ConfigService.getEnvConfig());
    }
    throw new HttpException('403 Forbidden', 403);
  }

  @Get('env')
  @ApiProduces('text/plain')
  getEnv( @Res() response) {
    if (this.config.get('DEV_GET_ENV_ENABLED')) {
      return response.json({ env: process.env });
    }
    throw new HttpException('403 Forbidden', 403);
  }  
}
