import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { DevController } from './dev.conroller';
import { DevService } from './dev.service';

@Module({
  imports: [ConfigModule, HttpModule],
  controllers: [DevController],
  providers: [DevService],
  exports: [DevService],
})

export class DevModule {
}
