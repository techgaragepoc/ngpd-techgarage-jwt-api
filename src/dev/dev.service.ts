import * as dotenv from 'dotenv';
import * as Joi from 'joi';
import * as fs from 'fs';
import { Injectable, HttpService, Header } from '@nestjs/common';
import { AxiosResponse, AxiosRequestConfig } from 'axios';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { TokenRequest } from './dto/token.request'
import { ConfigService } from '../config/config.service';

export interface EnvConfig {
  [key: string]: string;
}

@Injectable()
export class DevService {
  constructor(private readonly httpService: HttpService, private readonly configService: ConfigService) { }

  getHS256Token<T>(body: T, headers?: any) {
    return this.postServiceCall<T>(this.configService.get('JWT_HS256_ENDPOINT'), body, headers);
  }

  getRS256Token<T>(body: T) {
    return this.postServiceCall<T>(this.configService.get('JWT_RS256_ENDPOINT'), body);
  }

  postServiceCall<T>(endpoint: string, body: T, headers?: any) {
    if (headers == null) {
      //   headers = new Headers();
      //   // console.log('headers', headers);
      headers = {};
    }

    const apiKey = headers.apikey;

    if (apiKey === null || apiKey === undefined) {
      // headers.append("apikey", "2f7nGT30QZOO1FDCcpsU1F2PsZkisM5P");
      // console.log('body', body, 'headers', headers);
      headers.push({ "apikey": "2f7nGT30QZOO1FDCcpsU1F2PsZkisM5P" });
    }

    // console.log('headers', apiKey);

    // headers.append("Content-Type", "application/x-www-form-urlencoded");

    // [
    //   ,
    //   {
    //     "key": "apikey",
    //     "value": "2f7nGT30QZOO1FDCcpsU1F2PsZkisM5P",
    //     "type": "text"
    //   }
    // ];

    return this.httpService.post<T>(endpoint, body, { headers: headers })
      .pipe(map(x => x.data as T))
      .toPromise();
  }
}
