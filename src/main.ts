import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import * as csurf from 'csurf';
import * as session from 'express-session';
import * as bodyParser from 'body-parser';
import * as hpp from 'hpp';
import { ConfigService } from './config/config.service';
import { AllExceptionFilter } from './logger/AllExceptionFilter';
import { LoggerService } from './logger/logger.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new ConfigService();

  const options = new DocumentBuilder()
    .setTitle('Starter Kit BE API')
    .setVersion('1.0')
    .addTag('starter-kit-be')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  app.useGlobalFilters(new AllExceptionFilter(new LoggerService(config), config));

  app.use(session({
    name: 'sess_id',
    secret: config.get('APP_SESSION_SECRET'),
    resave: true,
    saveUninitialized: true,
    secure: true,
    cookie: {
      secure: config.get('APP_COOKIE_SECURE'),
      httpOnly: true,
      signed: true,
    },
  }));

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(hpp());

  app.enableCors({
    origin: config.get('APP_CORS'),
    credentials: true,
    exposedHeaders: [
      'Accept',
      'authorization',
      'Content-Type',
      'If-None-Match',
      'SourceType',
    ],
  });
  app.use(helmet());
  // app.use(csurf());
  // app.use(csurf({ cookie: false }));

  const port = parseInt(config.get('APP_PORT'), 10) || 3000;

  await app.listen(port);
}

bootstrap();
