import { Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { StatusController } from './status.controller';

@Module({
  imports: [ConfigModule],
  controllers: [StatusController],
})

export class StatusModule {
}
