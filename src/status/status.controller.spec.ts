import { Test, TestingModule } from '@nestjs/testing';
import { StatusController } from './status.controller';
import { ConfigService } from '../config/config.service';
import { ConfigModule } from '../config/config.module';

describe('StatusController', () => {
  const configService = { get: (key) => undefined };
  let statusController: StatusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      controllers: [StatusController],
    })
      .overrideProvider(ConfigService)
      .useValue(configService)
      .compile();

    statusController = module.get<StatusController>(StatusController);
  });

  describe('root', () => {
    it('should return "OK"', () => {
      expect(statusController.getHealthCheck()).toBe('OK');
    });

    it('should return app version', () => {
      const versionString = Math.random().toString();
      jest.spyOn(configService, 'get').mockImplementation((key) => {
        if (key === 'APP_VERSION_STRING') {
          return versionString;
        }
        return undefined;
      });
      expect(statusController.getAppVersion()).toBe(versionString);
    });
  });
});
