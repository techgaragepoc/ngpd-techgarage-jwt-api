import { Controller, Get } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { ApiProduces, ApiUseTags } from '@nestjs/swagger';

@Controller('api')
@ApiUseTags('status')
export class StatusController {
  constructor(private readonly configService: ConfigService) {
  }

  @Get('/health-check')
  @ApiProduces('text/plain')
  getHealthCheck(): string {
    return 'OK';
  }

  @Get('app-version')
  @ApiProduces('text/plain')
  getAppVersion(): string {
    return this.configService.get('APP_VERSION_STRING');
  }
}
