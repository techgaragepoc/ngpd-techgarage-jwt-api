import { Logger as LoggingAdapter } from 'ngpd-merceros-logging-adapter';
import { ConfigService } from '../config/config.service';
import * as winston from 'winston';
import { Injectable } from '@nestjs/common';

@Injectable()
export class LoggerService {

    private instantiated: boolean = false;

    constructor(private readonly configService: ConfigService) {
        if (!this.instantiated) {
            LoggingAdapter.init(
                this.configService.get('LOGGER_SERVICE'),
                {
                    level: this.configService.get('LOGGER_LEVEL'),
                    handleExceptions: this.configService.get('LOGGER_HANDLE_EXCEPTIONS'),
                },
            );
            this.instantiated = true;
        }
    }

    getCategory(name: string): any {
        return LoggingAdapter.getInstance(name);
    }

    error(name: string, message: string, correlationId?: string, meta?: object) {
        this.getCategory(name).error(...[message, correlationId, meta]);
    }

    info(name: string, message: string, correlationId?: string, meta?: object) {
        this.getCategory(name).info(...[message, correlationId, meta]);
    }

    warn(name: string, message: string, correlationId?: string, meta?: object) {
        this.getCategory(name).warn(...[message, correlationId, meta]);
    }
}
