import { Logger as LoggingAdapter } from 'ngpd-merceros-logging-adapter';
import { ConfigService } from '../config/config.service';
import { LoggerService } from './logger.service';

describe('LoggerService', () => {
  let configService: ConfigService;

  beforeEach(() => {
    configService = new ConfigService();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('constructor()', () => {
    test('should init Logger', () => {
      jest.spyOn(LoggingAdapter, 'init');

      const loggerService = new LoggerService(configService);
      expect(LoggingAdapter.init).toHaveBeenCalled();
      expect(loggerService).toBeInstanceOf(LoggerService);
    });
  });

  describe('getCategory()', () => {
    test('should return logger', () => {
      const loggerService = new LoggerService(configService);
      const name = Math.random().toString();
      const logger = Math.random().toString();

      jest.spyOn(LoggingAdapter, 'getInstance').mockImplementation(jest.fn().mockReturnValue(logger));
      const result = loggerService.getCategory(name);

      expect(LoggingAdapter.getInstance).toHaveBeenCalledWith(name);
      expect(result).toBe(logger);
    });
  });

  describe('error()', () => {
    test('should log an error', () => {
      const loggerService = new LoggerService(configService);
      const name = Math.random().toString();
      const message = Math.random().toString();
      const correlationId = Math.random().toString();
      const meta = {};

      jest.spyOn(LoggingAdapter.prototype, 'error');
      loggerService.error(name, message, correlationId, meta);

      expect(LoggingAdapter.prototype.error).toHaveBeenCalled();
    });
  });

  describe('info()', () => {
    test('should log an info', () => {
      const loggerService = new LoggerService(configService);
      const name = Math.random().toString();
      const message = Math.random().toString();
      const correlationId = Math.random().toString();
      const meta = {};

      jest.spyOn(LoggingAdapter.prototype, 'info');
      loggerService.info(name, message, correlationId, meta);

      expect(LoggingAdapter.prototype.info).toHaveBeenCalled();
    });
  });

  describe('warn()', () => {
    test('should log an warn', () => {
      const loggerService = new LoggerService(configService);
      const name = Math.random().toString();
      const message = Math.random().toString();
      const correlationId = Math.random().toString();
      const meta = {};

      jest.spyOn(LoggingAdapter.prototype, 'warn');
      loggerService.warn(name, message, correlationId, meta);

      expect(LoggingAdapter.prototype.warn).toHaveBeenCalled();
    });
  });
});
