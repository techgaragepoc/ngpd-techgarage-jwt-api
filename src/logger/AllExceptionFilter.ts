import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import * as os from 'os';
import { ConfigService } from '../config/config.service';
import { LoggerService } from './logger.service';

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  constructor(private readonly logger: LoggerService, private readonly config: ConfigService) {
    this.logger = logger;
    this.config = config;
  }

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest();
    const response = ctx.getResponse();
    this.responseToBrowser(this.config, exception, request, response);
    this.logToConsole(exception, request);
  }

  private responseToBrowser(config: ConfigService, error, request, response) {
    const body = {
      error: {
        status: error.status,
        message: error.message,
      },
    };

    this.applyCodeMessage(config, error, body);
    this.applyDetails(config, error, request, body);

    response.json(body);
    response.end();
  }

  private applyCodeMessage(config: ConfigService, error, body) {
    if (error.codeMessage) {
      body.error.codeMessage = error.codeMessage;

      if (error.name === 'ServiceApiError' && !config.get('ERROR_LOG_BROWSER_DETAILED')) {
        if (error.serviceName && error.serviceName === 'AuthenticationApi') {
          body.error.codeMessage = 'AUTHORIZATION_FAILED';
        } else {
          delete body.error.codeMessage;
        }
      }
    }
  }

  private applyDetails(config: ConfigService, error, request, body) {
    if (config.get('ERROR_LOG_BROWSER_DETAILED')) {
      body.details = {};
      body.user = request.user || null;
      body.stack = error.stack ? error.stack.split(os.EOL) : null;

      Object.keys(error).forEach((key) => {
        if (key !== 'code' && key !== 'name' && key !== 'message' && key !== 'codeMessage') {
          body.details[key] = error[key];
        }
      });
    }
  }

  private getCategoryNameByError(error) {
    switch (error.name) {
      case 'HttpError':
        return error.validationError ? 'validation' : 'http';
      case 'MongoError':
        return 'mongodb';
      default:
        return error.name;
    }
  }

  private logToConsole(error, request = null) {
    const extra = {
      stack: error.stack ? error.stack.split(os.EOL) : null,
      validationError: null,
    };
    const requestBodyAvailable = this.config.get('LOGGER_PRIVACY_LOG_REQUEST');

    if (request && !requestBodyAvailable) {
      request.body = null;
    }

    if (error.validationError) {
      extra.validationError = {
        message: error.validationError.message,
        path: error.validationError.path,
        type: error.validationError.type,
      };
    }

    if (request) {
      this.getCategory(this.getCategoryNameByError(error))
        .error(...this.prepare(error.message, request, extra));
    } else {
      this.getCategory(this.getCategoryNameByError(error))
        .error(error.message, extra);
    }
  }

  private getCategory(category) {
    return this.logger.getCategory(category);
  }

  private prepare(message: string, request, extra = null): any[] {
    const correlationId = request.isAuthenticated ? request.session.id : null;

    const meta = Object.assign({}, extra);
    meta.method = request.method;
    meta.url = request.url;
    meta.userAgent = request.headers['user-agent'] || 'Unknown';
    const referer = request.get('Referer');
    if (referer) {
      meta.referer = referer;
    }
    if (extra.body === null) {
      delete meta.body;
    } else if (request.body && !(Object.keys(request.body).length === 0 && request.body.constructor === Object)) {
      meta.body = request.body;
    }

    if (correlationId) {
      return [message, correlationId, meta];
    }

    return [message, meta];
  }
}
