import { Module } from '@nestjs/common';
import { LoggerService } from './logger.service';
import { ConfigModule } from '../config/config.module';
import { AllExceptionFilter } from './AllExceptionFilter';

@Module({
  providers: [LoggerService, AllExceptionFilter],
  exports: [LoggerService, AllExceptionFilter],
  imports: [ConfigModule],
})
export class LoggerModule {
}
